import abc
import json
import dataclasses
import pickle as pkl
from pathlib import Path
from typing import List

from .sample import Sample


class SampleSaver(abc.ABC):
    """
        Interface for Sample saver.
    """

    def __init__(self, output_dir: Path):
        self.output_dir = output_dir
        self.output_dir.mkdir(exist_ok=True)

    @abc.abstractmethod
    def _save(self, samples: List[Sample], filepath: Path) -> None:
        raise NotImplementedError('Method must be implemented in child class!')

    def get_output_filepath(self, filename: str) -> Path:
        return self.output_dir / filename

    def save(self, samples: List[Sample], filename: str) -> Path:
        output_filepath = self.get_output_filepath(filename=filename)
        self._save(samples=samples, filepath=output_filepath)
        return output_filepath


class PklSampleSaver(SampleSaver):
    """
        Save Samples as pickle file.
    """
    def _save(self, samples: List[Sample], filepath: Path) -> None:
        with filepath.open('wb') as file:
            pkl.dump(samples, file)


class JsonSampleSaver(SampleSaver):
    """
        Save Samples as json file.
    """
    def _save(self, samples: List[Sample], filepath: Path) -> None:
        with filepath.open('wb') as file:
            json.dump(list(map(dataclasses.asdict, samples)), file)
