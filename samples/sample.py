from dataclasses import dataclass


@dataclass
class Sample:
    header: str = ''
    body: str = ''
