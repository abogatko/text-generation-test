# Text Generation Test

## Quick start

Install requirements:
```
python -m pip install requirements.txt
```

Scrape data:
```
python scrap_data.py
```
