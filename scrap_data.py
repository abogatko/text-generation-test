import time
import requests
import argparse
from tqdm import tqdm
from lxml import etree
from io import StringIO
from pathlib import Path
from typing import List

from samples import Sample, SampleSaver, PklSampleSaver


class KremlinScrapper:
    """
        Class for scrapping congratulations from Kremlin website.
    """

    # Get from post: https://habr.com/ru/post/488720/
    # Purpose: site will thinks that the request is coming from a 'real' browser
    _HEADERS = {
        'cache-control': 'max-age=0',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/8.0 (Windows NT 10.0; Win64; x64) '
                      'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36',
        'sec-fetch-dest': 'document',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-mode': 'navigate'
    }

    def __init__(self, saver: SampleSaver = PklSampleSaver(output_dir=Path('outputs'))):
        self.base_url = 'http://kremlin.ru'
        self.saver = saver

    def _get_response(self, url: str) -> str:
        return requests.get(url=url, headers=self._HEADERS).text

    @staticmethod
    def _stop_condition(response: str) -> bool:
        return 'error' in response or 'denied' in response

    def _page_url_generator(self, page_id: int) -> str:
        return f'{self.base_url}/events/president/letters/page/{page_id}'

    @staticmethod
    def _get_html_tree(response: str) -> etree.ElementTree:
        return etree.parse(StringIO(response), etree.HTMLParser())

    def _get_congratulation_links(self, response: str) -> List[str]:
        """
            1. Get all page links
            2. Filter out links which not belongs to congratulation links
            3. Insert base url to filtered links
        """
        filtered_links = list(filter(
            lambda x: 'letters/' in x and 'page' not in x,
            KremlinScrapper._get_html_tree(response=response).xpath('//a/@href')
        ))
        return list(map(lambda x: f'{self.base_url}{x}',filtered_links))

    def _scrap_congratulation(self, link: str) -> Sample:
        time.sleep(1)  # Sleep for at least 1 second to avoid DDoS attack detection from server side

        response = self._get_response(url=link)
        tree = self._get_html_tree(response=response)

        # Scrap congratulation header and body
        congratulation_header = next(iter(tree.xpath('//h1[@itemprop="name"]/text()')))
        congratulation_body = '\n'.join(tree.xpath('//div[@itemprop="articleBody"]/p/text()'))

        return Sample(header=congratulation_header, body=congratulation_body)

    def __call__(self):
        last_response = ''
        page_id = 1  # Start from first page
        filename_pattern = '{page_id}.pkl'

        while not self._stop_condition(response=last_response):
            print(f'Page {page_id} in progress...')

            filename = filename_pattern.format(page_id=page_id)
            output_path = self.saver.get_output_filepath(filename=filename)
            # Skip scrapping if data from page already saved
            if output_path.exists():
                print(f'Skip scrapping from page {page_id}, already exists: {output_path}')
                page_id += 1
                continue

            # Get page url and page HTML
            url = self._page_url_generator(page_id=page_id)
            response = self._get_response(url=url)

            # Get congratulations links
            links = self._get_congratulation_links(response=response)

            # Scrap congratulations by links
            samples = [self._scrap_congratulation(link=link) for link in tqdm(links, desc='Scrap congratulations')]

            # Save results
            output_path = self.saver.save(samples=samples, filename=filename)

            print(f'Page {page_id} done. Saves to: {output_path}')

            last_response = response
            page_id += 1


def main(output_dir: Path) -> None:
    KremlinScrapper(saver=PklSampleSaver(output_dir=output_dir))()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Scrap congratulations from Kremlin website.')
    parser.add_argument('-o', '--output_dir', default=Path('outputs'), type=Path, required=False,
                        help='Output directory for saving scrapped data.')
    args = parser.parse_args()

    main(output_dir=args.output_dir)
